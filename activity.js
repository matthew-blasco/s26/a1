

// The questions are as follows:
// What directive is used by Node.js in loading the modules it needs?

    // Ans: To include a module, use the require() function with the name of the module: (e.g) var http = require('http');

// What Node.js module contains a method for server creation?

    // Ans: HTTP module : Now your application has access to the HTTP module, and is able to create a server: (e.g) http.createServer(function (req, res) {}).listen('port number');

// What is the method of the http object responsible for creating a server using Node.js?

    // Ans: HTTP.createServer(function (req, res) {}).listen('port number')

// What method of the response object allows us to set status codes and content types?

    // Ans: statusCode(``) && statusMessage; or also writeHead() & write()

// Where will console.log() output its contents when run in Node.js?

    // Ans: terminal

// What property of the request object contains the address' endpoint?

    // Ans: request.url